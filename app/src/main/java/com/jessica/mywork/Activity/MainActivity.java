package com.jessica.mywork.Activity;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.bottombar.sample.R;
import com.jessica.mywork.Other.DBHelper;
import com.jessica.mywork.Other.TabMessage;
import com.jessica.mywork.Fragment.AddTugasFragment;
import com.jessica.mywork.Fragment.JadwalFragment;
import com.jessica.mywork.Fragment.TugasFragment;
import com.jessica.mywork.Fragment.UserFragment;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.Objects;

public class MainActivity extends AppCompatActivity implements
        AddTugasFragment.OnFragmentInteractionListener,
        TugasFragment.OnFragmentInteractionListener,
        JadwalFragment.OnFragmentInteractionListener,
        UserFragment.OnFragmentInteractionListener{

    @SuppressLint("NewApi")
    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DBHelper DH = new DBHelper(this);
        SQLiteDatabase db = DH.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT id_group FROM users",null);

        if (cursor.getCount() <= 0){
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
        }else{
            cursor.moveToFirst();
            String idgroup = cursor.getString(0).toString();

            if (Objects.equals(idgroup, "")) {
                startActivity(new Intent(MainActivity.this, AddGroupActivity.class));
            }
        }

        BottomBar bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                changeFragment(TabMessage.get(tabId, false));
            }
        });

    }

    private void changeFragment(int position) {

        Fragment newFragment = null;

        switch (position){
            case 0:
                newFragment = new TugasFragment();
                break;
            case 1:
                newFragment = new AddTugasFragment();
                break;
            case 2:
                newFragment = new JadwalFragment();
                break;
            case 3:
                newFragment = new UserFragment();
                break;
        }

        getFragmentManager().beginTransaction().replace(
                R.id.fragmentContainer, newFragment)
                .commit();
    }

    @Override
    public void onTugasFragmentInteraction(Uri uri) {

    }

    @Override
    public void onAddTugasFragmentInteraction(Uri uri) {

    }

    @Override
    public void onUserFragmentInteraction(Uri uri) {

    }

    @Override
    public void onJadwalFragmentInteraction(Uri uri) {

    }
}
