package com.jessica.mywork.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bottombar.sample.R;
import com.jessica.mywork.Api.ApiClient;
import com.jessica.mywork.Api.ApiInterface;
import com.jessica.mywork.Api.UtilsApi;
import com.jessica.mywork.ObVar.Token;
import com.jessica.mywork.ObVar.User;
import com.jessica.mywork.Other.DBHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    ProgressDialog pd;
    Context mContext;
    EditText nama, email, password;
    Button btn_regis;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register);

        nama = (EditText) findViewById(R.id.nama);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        btn_regis = (Button) findViewById(R.id.btn_sign_up);
        mContext = this;

        pd = new ProgressDialog(this);
        pd.setMessage("Loading....");

        btn_regis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            pd.show();

            ApiInterface ApiInterface = UtilsApi.getApiService();

            String iemail = email.getText().toString();
            final String ipassword= password.getText().toString();
            final String inama = nama.getText().toString();

            Call<User> call = ApiInterface.registerRequest(inama, iemail, ipassword, ipassword);

            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    User data = response.body();

                    if (data.nama != ""){
                        DBHelper DH = new DBHelper(mContext);
                        SQLiteDatabase db = DH.getWritableDatabase();

                        db.execSQL("INSERT INTO users(nama, email, password, id_group) VALUES(\"" + data.nama + "\", \"" + data.email + "\",\"" + ipassword + "\",\"" + data.id_group + "\")");

                        ApiClient.resetToken(mContext, data.email, ipassword);

                        db.close();

                        pd.dismiss();

                        startActivity(new Intent(RegisterActivity.this, AddGroupActivity.class));
                        finish();
                    } else {
                        pd.dismiss();
                        String error_message = "Cek Koneksi Internet Kamu";
                        Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    pd.dismiss();
                    String error_message = "Cek Koneksi Internet Kamu";
                    Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();
                    Log.e("debug", "onFailure: ERROR > " + t.toString());
                }
            });
            }
        });
    }
}
