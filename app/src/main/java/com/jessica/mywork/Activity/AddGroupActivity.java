package com.jessica.mywork.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.bottombar.sample.R;
import com.jessica.mywork.Api.ApiClient;
import com.jessica.mywork.Api.ApiInterface;
import com.jessica.mywork.Api.UtilsApi;
import com.jessica.mywork.ObVar.User;
import com.jessica.mywork.Other.DBHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddGroupActivity extends AppCompatActivity {

    Context mContext;
    View mView;
    ProgressDialog pd;
    EditText input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group);
        mContext = this;
        mView = this.findViewById(android.R.id.content);
        pd = new ProgressDialog(mContext);
        ButterKnife.bind(AddGroupActivity.this);
    }

    @OnClick(R.id.btn_create_group)
    void CreateGroup(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        builder.setTitle("Create Group");
        View viewInflated = LayoutInflater.from(mContext).inflate(R.layout.dialog_create_group, (ViewGroup) mView, false);
        input = (EditText) viewInflated.findViewById(R.id.nm_group);
        builder.setView(viewInflated);

        // Set up the buttons
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                ActionCreateGroup(input.getText().toString());
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    @OnClick(R.id.btn_join_group)
    void JoinGroup(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        builder.setTitle("Create Group");
        View viewInflated = LayoutInflater.from(mContext).inflate(R.layout.dialog_join_group, (ViewGroup) mView, false);
        input = (EditText) viewInflated.findViewById(R.id.kd_group);
        builder.setView(viewInflated);

        // Set up the buttons
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                ActionJoinGroup(input.getText().toString());
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    void ActionCreateGroup(String nmGroup) {
        pd.show();
        ApiInterface ApiInterface = UtilsApi.getApiService();

        ApiClient.resetToken(mContext, "", "");
        String token = ApiClient.getToken(mContext);
        Call<String> call = ApiInterface.createGroup(token, nmGroup);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String data = response.body();
                System.out.println(data);

                if (data != ""){
                    DBHelper DH = new DBHelper(mContext);
                    SQLiteDatabase db = DH.getWritableDatabase();

                    db.execSQL("UPDATE users SET id_group = \"" + data + "\"");

                    ApiClient.resetToken(mContext, "", "");

                    db.close();

                    pd.dismiss();

                    startActivity(new Intent(AddGroupActivity.this, MainActivity.class));
                    finish();
                } else {
                    pd.dismiss();
                    String error_message = "Cek Koneksi Internet Kamu";
                    Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                pd.dismiss();
                String error_message = "Cek Koneksi Internet Kamu";
                Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();
                Log.e("debug", "onFailure: ERROR > " + t.toString());
            }
        });
    }

    void ActionJoinGroup(String kdGroup) {
        pd.show();
        ApiInterface ApiInterface = UtilsApi.getApiService();

        ApiClient.resetToken(mContext, "", "");
        String token = ApiClient.getToken(mContext);
        Call<String> call = ApiInterface.getIdGroupbyKd(token, kdGroup);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String data = response.body();
                System.out.println(data);

                if (data != ""){
                    DBHelper DH = new DBHelper(mContext);
                    SQLiteDatabase db = DH.getWritableDatabase();

                    db.execSQL("UPDATE users SET id_group = \"" + data + "\"");

                    ApiClient.resetToken(mContext, "", "");

                    db.close();

                    pd.dismiss();

                    startActivity(new Intent(AddGroupActivity.this, MainActivity.class));
                    finish();
                } else {
                    pd.dismiss();
                    String error_message = "Cek Koneksi Internet Kamu";
                    Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                pd.dismiss();
                String error_message = "Cek Koneksi Internet Kamu";
                Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();
                Log.e("debug", "onFailure: ERROR > " + t.toString());
            }
        });
    }
}
