package com.jessica.mywork.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bottombar.sample.R;
import com.jessica.mywork.Api.ApiInterface;
import com.jessica.mywork.Api.JadwalPelajaranItems;
import com.jessica.mywork.Api.TokenItems;
import com.jessica.mywork.Api.UtilsApi;
import com.jessica.mywork.ObVar.Hari;
import com.jessica.mywork.ObVar.Token;
import com.jessica.mywork.Other.DBHelper;

import java.util.List;

import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private TextView TvRegister;
    private Button btnSignIn;
    private EditText email;
    private EditText password;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final Context mContext = this;
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        pd = new ProgressDialog(this);
        pd.setMessage("Loading....");
        pd.setCancelable(false);

        TvRegister = (TextView) findViewById(R.id.tvcreateaccount);
        btnSignIn = (Button) findViewById(R.id.btn_sign_in);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);

        TvRegister.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.show();

                ApiInterface ApiInterface = UtilsApi.getApiService();

                String iemail = email.getText().toString();
                final String ipassword= password.getText().toString();

                Call<Token> call = ApiInterface.loginRequest(iemail, ipassword, true);

                call.enqueue(new Callback<Token>() {
                    @Override
                    public void onResponse(Call<Token> call, Response<Token> response) {
                        Token data = response.body();
                        pd.dismiss();

                        if (data != null){
                            DBHelper DH = new DBHelper(mContext);
                            SQLiteDatabase db = DH.getWritableDatabase();

                            Cursor cursor = db.rawQuery("SELECT id FROM token",null);

                            if (cursor.getCount() <= 0) {
                                db.execSQL("INSERT INTO token(content, type) VALUES(\""+ data.token +"\", \"" + data.token_type+ "\")");
                            }else{
                                cursor.moveToFirst();
                                String id = cursor.getString(0);

                                db.execSQL("UPDATE token SET content=\""+ data.token +"\", type=\"" + data.token_type+ "\" WHERE id = "+ id +"");
                            }

                            cursor.close();

                            if(data.user.id_group == null){
                                data.user.id_group = "";
                            }

                            db.execSQL("INSERT INTO users(nama, email, password, id_group) VALUES(\"" + data.user.nama + "\", \"" + data.user.email + "\",\"" + ipassword + "\",\"" + data.user.id_group + "\")");

                            cursor.close();
                            db.close();

                            if (data.user.id_group != "") {
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            }else{
                                startActivity(new Intent(LoginActivity.this, AddGroupActivity.class));
                            }
                            finish();
                        } else {
                            String error_message = "Cek Koneksi Internet Kamu";
                            Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Token> call, Throwable t) {
                        pd.dismiss();
                        String error_message = "Cek Koneksi Internet Kamu";
                        Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();
                        Log.e("debug", "onFailure: ERROR > " + t.toString());
                    }
                });
            }
        });



    }
}
