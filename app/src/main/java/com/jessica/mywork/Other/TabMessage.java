package com.jessica.mywork.Other;

import com.example.bottombar.sample.R;

/**
 * Created by iiro on 7.6.2016.
 */
public class TabMessage {
    public static Integer get(int menuItemId, boolean isReselection) {
        Integer position = null;

        switch (menuItemId) {
            case R.id.tab_home:
                position = 0;
                break;
            case R.id.tab_add:
                position = 1;
                break;
            case R.id.tab_jadwal:
                position = 2;
                break;
            case R.id.tab_user:
                position = 3;
                break;
        }

        return position;
    }
}
