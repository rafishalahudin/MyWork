package com.jessica.mywork.Other;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by Jessica on 9/5/17.
 */

public class DBHelper extends SQLiteOpenHelper {
    private static String DATABASE_NAME="mywork.db";
    private static Integer DATABASE_VERSION=1;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE `hari` (\n" +
                "  `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "  `namahari` VARCHAR(255) NOT NULL\n" +
                ");");

        db.execSQL("CREATE TABLE `matapelajaran` (\n" +
                "  `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "  `nmmatapelajaran` VARCHAR(255) NOT NULL,\n" +
                "  `namaguru` VARCHAR(255) NOT NULL,\n" +
                "  `keterangan` VARCHAR(255) NOT NULL,\n" +
                "  `created_at` TIMESTAMP DEFAULT NULL\n" +
                ");");

        db.execSQL("CREATE TABLE `statuspengerjaan` (\n" +
                "  `id` INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,\n" +
                "  `iduser` INTEGER NOT NULL,\n" +
                "  `status` INTEGER NOT NULL,\n" +
                "  `idtugas` INTEGER NOT NULL\n" +
                ");");

        db.execSQL("CREATE TABLE `tugas` (\n" +
                "  `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "  `judul` VARCHAR(255) NOT NULL,\n" +
                "  `idmatapelajaran` INTEGER NOT NULL,\n" +
                "  `keterangan` VARCHAR(255) NOT NULL,\n" +
                "  `file` VARCHAR(255) NOT NULL,\n" +
                "  `tglpengumpulan` DATE DEFAULT NULL,\n" +
                "  `created_at` TIMESTAMP DEFAULT NULL\n" +
                ");");

        db.execSQL("CREATE TABLE `users` (\n" +
                "  `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "  `nama` VARCHAR(255) NOT NULL,\n" +
                "  `email` VARCHAR(255) UNIQUE NOT NULL,\n" +
                "  `password` VARCHAR(255) NOT NULL,\n" +
                "  `foto` text,\n" +
                "  `bio` VARCHAR(255) DEFAULT NULL,\n" +
                "  `remember_token` VARCHAR(100) DEFAULT NULL,\n" +
                "  `id_group` INTEGER\n" +
                ");");


        db.execSQL("CREATE TABLE `token` (\n" +
                "  `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "  `content` VARCHAR(255) NOT NULL,\n" +
                "  `type` VARCHAR(255) NOT NULL\n" +
                ");");

        Log.d("Data", "Table Created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS hari");
        db.execSQL("DROP TABLE IF EXISTS matapelajaran");
        db.execSQL("DROP TABLE IF EXISTS statuspengerjaan");
        db.execSQL("DROP TABLE IF EXISTS users");
        db.execSQL("DROP TABLE IF EXISTS tugas");
        db.execSQL("DROP TABLE IF EXISTS token");
        onCreate(db);
    }


}
