package com.jessica.mywork.ObVar;

/**
 * Created by rafishalahudin on 21/01/18.
 */

public class TugasDao {

    private String tanggal_tugas;
    private String bulan_tugas;
    private String lblnamatugas;
    private String lblmatpel;

    public String getTanggal_tugas() {
        return tanggal_tugas;
    }

    public void setTanggal_tugas(String tanggal_tugas) {
        this.tanggal_tugas = tanggal_tugas;
    }

    public String getBulan_tugas() {
        return bulan_tugas;
    }

    public void setBulan_tugas(String bulan_tugas) {
        this.bulan_tugas = bulan_tugas;
    }

    public String getLblnamatugas() {
        return lblnamatugas;
    }

    public void setLblnamatugas(String lblnamatugas) {
        this.lblnamatugas = lblnamatugas;
    }

    public String getLblmatpel() {
        return lblmatpel;
    }

    public void setLblmatpel(String lblmatpel) {
        this.lblmatpel = lblmatpel;
    }

    public TugasDao(String tanggal_tugas, String bulan_tugas, String lblnamatugas, String lblmatpel) {

        this.tanggal_tugas = tanggal_tugas;
        this.bulan_tugas = bulan_tugas;
        this.lblnamatugas = lblnamatugas;
        this.lblmatpel = lblmatpel;
    }
}
