package com.jessica.mywork.ListAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bottombar.sample.R;
import com.jessica.mywork.ObVar.TugasDao;

import java.util.List;

/**
 * Created by rafishalahudin on 21/01/18.
 */

public class TugasAdapter extends RecyclerView.Adapter<TugasAdapter.ViewHolder> {

    private String[] tanggal_tugas = {
            "2",
            "4",
            "5",
            "6",
            "7",
            "8"
    };
    private String[] bulan_tugas = {
            "Januari",
            "Februari",
            "Maret",
            "April",
            "April",
            "Mei",
    };
    private String[] namatugas = {
            "Rangkum",
            "Kerjakan",
            "Diemin",
            "Kerjakan",
            "Kerjakan",
            "Rangkum"
    };
    private String[] matpel = {
            "Sejarah",
            "Matematika",
            "Sunda",
            "Indonesia",
            "SBK",
            "PWD",
    };

    private Context context;
    private List<TugasDao> tugasDaoList;

    public TugasAdapter(Context context, List<TugasDao> tugasDaoList) {
        this.context = context;
        this.tugasDaoList = tugasDaoList;
    }

    @Override
    public TugasAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tugas_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(TugasAdapter.ViewHolder holder, int position) {
        holder.tanggal_tugas.setText(tanggal_tugas[position]);
        holder.bulan_tugas.setText(bulan_tugas[position]);
        holder.lblnamapelajaran.setText(namatugas[position]);
        holder.lblmatpel.setText(matpel[position]);
    }

    @Override
    public int getItemCount() {
        return tanggal_tugas.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tanggal_tugas, bulan_tugas, lblnamapelajaran, lblmatpel;

        public ViewHolder(View itemView) {
            super(itemView);
            tanggal_tugas = (TextView) itemView.findViewById(R.id.tanggal_tugas);
            bulan_tugas = (TextView) itemView.findViewById(R.id.bulan_tugas);
            lblnamapelajaran = (TextView) itemView.findViewById(R.id.lblNamaTugas);
            lblmatpel = (TextView) itemView.findViewById(R.id.lblMatpel);

        }
    }
}
