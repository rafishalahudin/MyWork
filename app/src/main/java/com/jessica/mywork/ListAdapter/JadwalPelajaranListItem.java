package com.jessica.mywork.ListAdapter;

/**
 * Created by Jessica on 08/01/18.
 */

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.bottombar.sample.R;

import java.util.HashMap;
import java.util.List;

public class JadwalPelajaranListItem extends BaseExpandableListAdapter{

    private Context context;
    private List<String> listDataHari;
    private HashMap<String, List<String>> listDataPelajaran;

    public JadwalPelajaranListItem(Context context,List<String> listDataHari,
                           HashMap<String, List<String>> listDataPelajaran){
        this.context = context;
        this.listDataHari = listDataHari;
        this.listDataPelajaran = listDataPelajaran;
    }
    @Override
    public int getGroupCount() {
        return this.listDataHari.size();
        // get hari count
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.listDataPelajaran.get(this.listDataHari.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listDataHari.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.listDataPelajaran.get(this.listDataHari.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String Namahari = (String) getGroup(groupPosition);
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_hari, null);
        }

        TextView ListHari = (TextView) convertView.findViewById(R.id.ListHari);
        ListHari.setTypeface(null, Typeface.BOLD);
        ListHari.setText(Namahari);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String pelajaranText = (String) getChild(groupPosition, childPosition);

        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_pelajaran, null);
        }

        TextView tvListPelajaran = (TextView) convertView.findViewById(R.id.ListPelajaran);
        tvListPelajaran.setText(pelajaranText);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}