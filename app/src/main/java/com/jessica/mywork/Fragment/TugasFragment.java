package com.jessica.mywork.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bottombar.sample.R;
import com.jessica.mywork.ListAdapter.TugasAdapter;
import com.jessica.mywork.ObVar.TugasDao;

import java.util.ArrayList;
import java.util.List;


public class TugasFragment extends Fragment {

    private RecyclerView rvView;
    private RecyclerView.Adapter adapter;
    private List<TugasDao> tugasDaoList;
    private OnFragmentInteractionListener mListener;
    public static TugasFragment newInstance(String param1, String param2) {
        TugasFragment fragment = new TugasFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tugas, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        rvView = (RecyclerView) getActivity().findViewById(R.id.tugasrv);
        rvView.setHasFixedSize(true);
        rvView.setLayoutManager(new LinearLayoutManager(getActivity()));

        tugasDaoList = new ArrayList<>();

        adapter = new TugasAdapter(getActivity(),tugasDaoList);

        rvView.setAdapter(adapter);

    }

    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface OnFragmentInteractionListener {
        public void onTugasFragmentInteraction(Uri Uri);
    }
}
