package com.jessica.mywork.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.example.bottombar.sample.R;
import com.jessica.mywork.Api.ApiClient;
import com.jessica.mywork.Api.ApiInterface;
import com.jessica.mywork.Api.UtilsApi;
import com.jessica.mywork.ObVar.Hari;
import com.jessica.mywork.Api.JadwalPelajaranItems;
import com.jessica.mywork.ListAdapter.JadwalPelajaranListItem;
import com.jessica.mywork.ObVar.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class JadwalFragment extends Fragment {
    JadwalPelajaranListItem listAdapter;
    ExpandableListView expandableListView;
    List<String> listDataHari;
    HashMap<String, List<String>> listDataPelajaran;
    View view;


    private OnFragmentInteractionListener mListener;

    public JadwalFragment() {

    }

    public static JadwalFragment newInstance() {
        JadwalFragment fragment = new JadwalFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void loadData(List<Hari> hari) {

        for (Hari data : hari) {
            Log.d("Test","Nama Hari : " + data.namahari);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_jadwal, container, false);

        this.prepareListData();

        expandableListView = (ExpandableListView) this.view.findViewById(R.id.ExpLVJadwal);

        listAdapter = new JadwalPelajaranListItem(getActivity(), listDataHari, listDataPelajaran);

        expandableListView.setAdapter(listAdapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int hariPosition, long id) {
                return false;
            }
        });

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int hariPosition) {

            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int hariPosition) {

            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                return false;
            }
        });

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onJadwalFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onJadwalFragmentInteraction(Uri uri);
    }

    private boolean prepareListData(){
        listDataHari = new ArrayList<>();
        listDataPelajaran = new HashMap<>();

        listDataHari.add("Senin");
        listDataHari.add("Selasa");
        listDataHari.add("Rabu");
        listDataHari.add("Kamis");
        listDataHari.add("Jumat");
        listDataHari.add("Sabtu");

        List<String> Senin = new ArrayList<>();
        Senin.add("Pelajaran 1");
        Senin.add("Pelajaran 2");
        Senin.add("Pelajaran 3");
        Senin.add("Pelajaran 4");

        List<String> Selasa = new ArrayList<>();
        Selasa.add("Pelajaran 1");
        Selasa.add("Pelajaran 2");
        Selasa.add("Pelajaran 3");
        Selasa.add("Pelajaran 4");

        List<String> Rabu = new ArrayList<>();
        Rabu.add("Pelajaran 1");
        Rabu.add("Pelajaran 2");
        Rabu.add("Pelajaran 3");
        Rabu.add("Pelajaran 4");

        List<String> Kamis = new ArrayList<>();
        Kamis.add("Pelajaran 1");
        Kamis.add("Pelajaran 2");
        Kamis.add("Pelajaran 3");
        Kamis.add("Pelajaran 4");

        List<String> Jumat = new ArrayList<>();
        Jumat.add("Pelajaran 1");
        Jumat.add("Pelajaran 2");
        Jumat.add("Pelajaran 3");
        Jumat.add("Pelajaran 4");

        List<String> Sabtu = new ArrayList<>();
        Sabtu.add("Pelajaran 1");
        Sabtu.add("Pelajaran 2");
        Sabtu.add("Pelajaran 3");
        Sabtu.add("Pelajaran 4");

        listDataPelajaran.put(listDataHari.get(0), Senin);
        listDataPelajaran.put(listDataHari.get(1), Selasa);
        listDataPelajaran.put(listDataHari.get(2), Rabu);
        listDataPelajaran.put(listDataHari.get(3), Kamis);
        listDataPelajaran.put(listDataHari.get(4), Jumat);
        listDataPelajaran.put(listDataHari.get(5), Sabtu);

        return true;
    }
}
