package com.jessica.mywork.Api;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.jessica.mywork.ObVar.Token;
import com.jessica.mywork.Other.DBHelper;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Jessica on 07/01/18.
 */

public class ApiClient {

    public static final String Base_URL = "http://192.168.100.90:8000/api/";

    public static Retrofit retrofit = null;

    public static Retrofit getClient(){
        if (retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(Base_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static String getToken(Context context){
        DBHelper DH = new DBHelper(context);
        SQLiteDatabase db = DH.getWritableDatabase();

        Cursor cursor = db.rawQuery("SELECT content, type FROM token",null);

        String data = "";

        if (cursor.getCount() > 0){
            cursor.moveToFirst();
            data = cursor.getString(1) + " " + cursor.getString(0);
        }

        cursor.close();
        db.close();

        return data;
    }

    public static boolean resetToken(final Context context, String email, String password){
        DBHelper DH = new DBHelper(context);
        final SQLiteDatabase db = DH.getWritableDatabase();
        final boolean[] kon = {true};

        if (email == "" && password == ""){
            Cursor cursor = db.rawQuery("SELECT email, password FROM users",null);
            if (cursor.getCount() <= 0) {
                cursor.moveToFirst();
                email = cursor.getString(0);
                password = cursor.getString(1);
            }else{
                kon[0] = false;
            }
        }

        ApiInterface ApiInterface = UtilsApi.getApiService();
        Call<Token> call = ApiInterface.loginRequest(email, password, true);

        call.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                Token data = response.body();

                if (data.token != "") {
                    Cursor cursor = db.rawQuery("SELECT id FROM token", null);

                    if (cursor.getCount() <= 0) {
                        db.execSQL("INSERT INTO token(content, type) VALUES(\"" + data.token + "\", \"" + data.token_type + "\")");
                    } else {
                        cursor.moveToFirst();
                        String id = cursor.getString(0);

                        db.execSQL("UPDATE token SET content=\"" + data.token + "\", type=\"" + data.token_type + "\" WHERE id = " + id + "");
                    }
                    cursor.close();
                    db.close();
                } else {
                    kon[0] = false;
                    String error_message = "Cek Koneksi Internet Kamu";
                    Toast.makeText(context, error_message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                String error_message = "Cek Koneksi Internet Kamu";
                Toast.makeText(context, error_message, Toast.LENGTH_SHORT).show();
                Log.e("debug", "onFailure: ERROR > " + t.toString());
                kon[0] = false;
            }
        });

        return true;
    }
}
