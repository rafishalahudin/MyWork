package com.jessica.mywork.Api;

/**
 * Created by Jessica on 14/01/18.
 */

public class UtilsApi {

    public static ApiInterface getApiService(){
        return ApiClient.getClient().create(ApiInterface.class);
    }

}
