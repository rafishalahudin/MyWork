package com.jessica.mywork.Api;

import com.google.gson.annotations.SerializedName;
import com.jessica.mywork.ObVar.Token;

import java.util.List;

/**
 * Created by Jessica on 15/01/18.
 */

public class TokenItems {
    @SerializedName("items")
    public Token items;

    public Token getItems() { return items; }

    public void setItems(Token items) { this.items = items; }

    public TokenItems(Token items) { this.items = items; }
}
