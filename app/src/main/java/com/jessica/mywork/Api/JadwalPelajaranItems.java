package com.jessica.mywork.Api;

import com.google.gson.annotations.SerializedName;
import com.jessica.mywork.ObVar.Hari;

import java.util.List;

/**
 * Created by Jessica on 07/01/18.
 */

public class JadwalPelajaranItems {

    @SerializedName("items")
    public List<Hari> items;

    public List<Hari> getItems() { return items; }

    public void setItems(List<Hari> items) { this.items = items; }

    public JadwalPelajaranItems(List<Hari> items) { this.items = items; }

}
