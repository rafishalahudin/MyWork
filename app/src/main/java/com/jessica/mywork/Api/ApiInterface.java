package com.jessica.mywork.Api;

import com.jessica.mywork.ObVar.Token;
import com.jessica.mywork.ObVar.User;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Jessica on 07/01/18.
 */

public interface ApiInterface {


    @GET("user")
    Call<User> getUser(@Header("Authorization") String auth);

    @GET("graphql/query")
    Call<JadwalPelajaranItems> getJadwalPelajaran(@Header("Authorization") String auth, @Query("query") String query);

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("login")
    Call<Token> loginRequest(@Field("email") String email,
                             @Field("password") String password,
                             @Field("remember") Boolean remember);

    @FormUrlEncoded
    @POST("register")
    Call<User> registerRequest(@Field("nama") String nama,
                               @Field("email") String email,
                               @Field("password") String password,
                               @Field("password_confirmation") String password_confirmation);

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("group")
    Call<String> createGroup(@Header("Authorization") String auth, @Field("nama_group") String nama_group);

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("group/getIdGroupByKd")
    Call<String> getIdGroupbyKd(@Header("Authorization") String auth, @Field("kd_group") String kd_group);

}
